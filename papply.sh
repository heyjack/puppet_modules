#!/bin/sh
PUPPETDIR=$HOME/puppet_modules
sudo /usr/bin/puppet apply --modulepath ${PUPPETDIR}/modules ${PUPPETDIR}/manifests/site.pp --no-report
