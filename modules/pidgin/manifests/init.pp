#Pidgin install and configuration
class pidgin ($user_name = 'UNSET',) {

#default value
$_user_name = $user_name ? {
  'UNSET' => 'username',
  default => user_name,
}

package { 'pidgin': ensure => installed }

file { "/home/${user_name}/.purple":
  ensure  => 'directory',
  owner   => $user_name,
  mode    => '0700',
}

file { "/home/${user_name}/.purple/accounts.xml":
  replace => 'no',
  owner   => $user_name,
  mode    => '0600',
  content => template('pidgin/accounts.xml'),
}

}
